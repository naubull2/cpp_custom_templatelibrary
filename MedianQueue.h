#pragma once
/*
  A simple priority queue where the median is the top priority element
	Implemented by balanceing a max heap and a min heap
		- max heap contains the elements less than or equal to the median
		- min heap contains the elements greater than the median
*/
#include <iostream>
#include <queue>
#include <functional>

template <class item>
class MedianQueue
{
private:
	std::priority_queue<item> maxQ; // keeps the smaller half
	std::priority_queue<item, std::vector<item>, std::greater<item>> minQ; // keeps the greater half
	bool isOdd;
	int size;

public:
	MedianQueue();
	// MedianQueue(item* arr, int size);

	// bool balance(); to be used in the constructor that is initialized with an existing array of items
	bool push(item input);
	item pop();
	const item peek();
	int getsize(){ return this->size; }

	//void print_queue();
};
template <class item>
MedianQueue<item>::MedianQueue(){
	int size = 0;
	isOdd = false;
}

template <class item>
bool MedianQueue<item>::push(item input){
	isOdd = !isOdd;
	++size;
	// push element to the smaller queue, if the same, push to the maxQ
	if (maxQ.size() > minQ.size())
		minQ.push(input);
	else
		maxQ.push(input);

	// swap the top elements if the median condition is broken
	if (size == 1) return true;
	if (maxQ.top() > minQ.top()){
		item max = maxQ.top();
		item min = minQ.top();
		maxQ.pop();
		minQ.pop();
		maxQ.push(min);
		minQ.push(max);
	}
	return true;
}

template <class item>
item MedianQueue<item>::pop(){
	isOdd = !isOdd;
	// pop from maxQ
	if (size == 0){
		std::cout << "NO ITEM" << std::endl;
		return 0;
	}
	item ret = maxQ.top();
	maxQ.pop();
	--size;
	// if minQ is larger, transfer one element from minQ to maxQ
	if (minQ.size() > maxQ.size()){
		item min = minQ.top();
		minQ.pop();
		maxQ.push(min);
	}								 
	return ret;
}

template <class item>
const item MedianQueue<item>::peek(){
	return maxQ.top();
}


