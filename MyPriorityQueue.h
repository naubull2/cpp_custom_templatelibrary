#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>

template <class item> // item class must have the comparison operators and the copy constructor overloaded
class MyPriorityQueue
{
private:
	const static int default_size;
	item *Queue;
	int size;
	int last_index = -1; // last index used for pop operation

	bool max_heapify(int index);	// adjust to satisfy the max heap condition 
	void swap_item(item& i1, item& i2);

public:
	MyPriorityQueue();
	MyPriorityQueue(int size);		// initialize with the given size
	MyPriorityQueue(item* arr, int size);		// turn an existing array into a priority queue

	int push(item input);					// returns remaining capacity of the heap
	const item peek();	// returns the top priority item, doesn't modify the heap
	item pop();		// returns the top priority item, reorder the heap

	void print_queue();	// visualize queue
};

class HeapSizeException : public std::runtime_error{
public:
	HeapSizeException() : runtime_error("Heap size error"){}

	virtual const char* what() const throw(){
		return "Heap size is not valid";
	}
};

class HeapUnderflowException : public std::runtime_error{
public:
	HeapUnderflowException() : runtime_error("Heap underflow"){}

	virtual const char* what() const throw(){
		return "Heap underflow exception";
	}
};

template <class item>
bool MyPriorityQueue<item>::max_heapify(int index)
{ // build max heap from the index whose sub trees are already max heaps
	if (index > last_index)
		throw HeapSizeException();
	int left = 2 * index + 1;
	int right = 2 * index + 2;

	int max = index;

	if (left <= last_index && Queue[left] > Queue[max]){
		max = left;
	}
	if (right <= last_index && Queue[right] > Queue[max]){
		max = right;
	}
	if (max == index)	// this is already a max heap
		return true;
	else if (max == left)
		swap_item(Queue[left], Queue[index]);
	else
		swap_item(Queue[right], Queue[index]);
	return max_heapify(max);	// sift down if any change is made
}

template <class item>
MyPriorityQueue<item>::MyPriorityQueue()
{
	size = 2;
	Queue = new item[2];	// default heap size is 2
}
template <class item>
MyPriorityQueue<item>::MyPriorityQueue(int size)
{
	// must satisfy, size >= 1
	if (size < 1)
		throw HeapSizeException();

	this->size = size;
	Queue = new item[size];
}

template <class item>
MyPriorityQueue<item>::MyPriorityQueue(item* arr, int size)
{
	// must satisfy, size >= 1
	if (size < 1)
		throw HeapSizeException();

	this->size = size;
	Queue = new item[size];
	for (int i = 0; i < size; ++i)
		Queue[i] = arr[i];
	last_index = size - 1;

	// tight bound of O(N) building a max heap
	for (int i = (size - 2) / 2; i >= 0; --i)
		max_heapify(i);
}

template <class item>
int MyPriorityQueue<item>::push(item input)
{
	if (last_index == size - 1){
		// double the heap space
		item* tmp = new item[size * 2];
		for (int i = 0; i < size; ++i)
			tmp[i] = Queue[i];
		delete Queue;
		Queue = tmp;
	}
	++last_index;
	Queue[last_index] = input;
	// sift up through the heap	
	for (int i = last_index; i >= 0; i = (i - 1) / 2){
		if (Queue[i] > Queue[(i - 1) / 2])
			swap_item(Queue[i], Queue[(i - 1) / 2]);
		else
			break;
	}
	return size - last_index - 1; // returns the remaining capacity of the heap
}

template <class item>
const item   MyPriorityQueue<item>::peek()
{
	return   Queue[0];
}

template <class item>
void MyPriorityQueue<item>::swap_item(item& i1, item& i2)
{
	item tmp = i1;
	i1 = i2;
	i2 = tmp;
}

template <class item>
item MyPriorityQueue<item>::pop()
{
	if (last_index == -1)	// heap underflow
		throw HeapUnderflowException();
	item ret = Queue[0];
	swap_item(Queue[0], Queue[last_index]);
	--last_index;
	// heapify at the root
	if (last_index >= 0)
		max_heapify(0);
	return ret;
}

template <class item>
void MyPriorityQueue<item>::print_queue()
{
	int i = 1;
	if (last_index < 0)
		return;	// empty queue, nothing to print
	while ((i - 1) <= last_index)
		i *= 2;
	i /= 2;
	int j = 1;
	int print = 0;
	while (j <= i){
		for (int k = 0; k < j; ++k){
			if (print <= last_index)
				std::cout << Queue[print++] << " ";
		}
		std::cout << std::endl;
		j *= 2;
	}
}